#import <UIKit/UIKit.h>

static BOOL removeBlur;

static BOOL isiPadPro() {
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] nativeBounds].size.height == 2732;
}

static CGRect iconFrameForGridIndex(NSUInteger index) {
    CGFloat iconSize;
    
    if (isiPadPro()) {
        iconSize = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 60 : 84;
    } else {
        iconSize = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 60 : 77;
    }
    
    CGFloat iconMargin = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 3 : 6;
    if (index == 0) {
        return CGRectMake(iconSize, 0, iconSize, iconSize);
    } else {
        return CGRectMake(iconSize / 2, iconSize + iconMargin, 0, 0);
    }
}

%hook SBIconGridImage
+ (CGRect)rectAtIndex:(unsigned long long)index maxCount:(unsigned long long)arg2
{
    if (removeBlur) {
        return iconFrameForGridIndex(index);
    } else {
        return %orig;
    }
}

+ (CGSize)cellSize
{
    if (removeBlur) {
        if (isiPadPro()) {
            return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? CGSizeMake(58, 18) : CGSizeMake(60.5, 18.5);
        } else {
            return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? CGSizeMake(58, 18) : CGSizeMake(55.8, 16.8);
        }
    } else {
        return %orig;
    }
}
%end

static void preferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo)
{
    NSDictionary *preferences = [NSDictionary dictionaryWithContentsOfFile:@"/var/mobile/Library/Preferences/me.qusic.taptapfolder.plist"];
    removeBlur = [preferences[@"blurBeGone"]boolValue];
}

%ctor {
    @autoreleasepool {
        CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, preferencesChangedCallback, CFSTR("me.qusic.taptapfolder.preferencesChanged"), NULL, CFNotificationSuspensionBehaviorCoalesce);
        preferencesChangedCallback(NULL, NULL, NULL, NULL, NULL);
    }
}